package runner

import (
	"log"
	"strings"

	"github.com/gammazero/nexus/v3/wamp"
	"github.com/gempir/go-twitch-irc/v2"
	"gitlab.com/livesocket/service"
	"gitlab.com/livesocket/super-command-runner/helpers"
)

// RunSuperCommand Run the super command and reply if necessary
func RunSuperCommand(message *twitch.PrivateMessage) {
	// Process arguments
	slice := strings.Split(message.Message, " ")
	parts := slice[1:len(slice)]
	command := parts[0]

	// Check if user is admin
	isAdmin, err := helpers.IsAdmin(message.User.Name)
	if err != nil {
		log.Print(err)
		return
	}

	// If user is not an admin, stop processing
	if !isAdmin {
		return
	}

	// Find super command to run
	super, err := helpers.FindSuperCommand(command)
	if err != nil {
		log.Print(err)
		return
	}
	if super == nil {
		return
	}

	// Convert message into args
	rest := parts[1:len(parts)]
	args := wamp.List{}
	for _, item := range rest {
		args = append(args, item)
	}

	// Create kwargs for command
	kwargs := wamp.Dict{"message": message}

	// Run Super Command
	result, err := service.Socket.SimpleCall(super.Proc, args, kwargs)
	if err != nil {
		log.Print(err)
		return
	}
	// If something to say, speak message
	if result != nil && len(result.Arguments) > 0 && result.Arguments[0].(string) != "" {
		err := helpers.Speak(message.Channel, result.Arguments[0].(string))
		if err != nil {
			log.Print(err)
			return
		}
	}
}

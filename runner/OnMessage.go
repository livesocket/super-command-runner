package runner

import (
	"log"
	"regexp"

	"github.com/gammazero/nexus/v3/wamp"
	"github.com/gempir/go-twitch-irc/v2"
	"gitlab.com/livesocket/service"
	"gitlab.com/livesocket/service/lib"
	"gitlab.com/livesocket/super-command-runner/helpers"
)

// OnMessageSubscription Handler for detection and execution of super commands in chat messages
var OnMessageSubscription = lib.Subscription{
	Topic:   "event.chat.message",
	Handler: onMessage,
}

func onMessage(event *wamp.Event) {
	// If message exists
	if len(event.Arguments) > 0 {
		// Convert json message to twitch.PrivateMessage
		message := service.MapToPrivateMessage(event.Arguments[0].(map[string]interface{}))

		// Process message
		go processMessage(message)
	}
}

func processMessage(message *twitch.PrivateMessage) {
	// Get channel record
	result, err := helpers.GetChannel(message.Channel)
	if err != nil {
		log.Print(err)
		return
	}
	// If no channel is found
	if result == nil {
		log.Printf("%s channel not found", message.Channel)
	}
	// Check if message is super command
	regex := regexp.MustCompile(`^@?SnareChopsBot`)
	if regex.MatchString(message.Message) {
		log.Print("Super Command Detected")
		go RunSuperCommand(message)
	}
}

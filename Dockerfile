FROM golang:alpine as base
RUN apk --no-cache add git ca-certificates g++
WORKDIR /repos/super-command-runner
ADD go.mod go.sum ./
RUN go mod download

FROM base as builder
WORKDIR /repos/super-command-runner
ADD . .
RUN CGO_ENABLED=0 GOOS=linux go build -o super-command-runner

FROM scratch as release
COPY --from=builder /repos/super-command-runner/super-command-runner /super-command-runner
EXPOSE 8080
ENTRYPOINT ["/super-command-runner"]

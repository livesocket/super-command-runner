package helpers

import (
	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/service"
)

// Channel The important bits of a channel
type Channel struct {
	Name    string
	BotName string
}

// GetChannel WAMP call helper for getting a channel by name
func GetChannel(name string) (*Channel, error) {
	// Call get channel by name endpoint
	res, err := service.Socket.SimpleCall("private.channel.get", nil, wamp.Dict{"name": name})
	if err != nil {
		return nil, err
	}
	// Check and return response
	if len(res.Arguments) > 0 && res.Arguments[0] != nil {
		channel := res.Arguments[0].(map[string]interface{})
		return &Channel{
			Name:    channel["name"].(string),
			BotName: channel["bot_name"].(string),
		}, nil
	}
	return nil, nil
}

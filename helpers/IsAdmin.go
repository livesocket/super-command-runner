package helpers

import (
	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/service"
)

// IsAdmin WAMP call helper to check if username is admin
func IsAdmin(username string) (bool, error) {
	// Call admin get endpoint
	res, err := service.Socket.SimpleCall("private.admin.get", nil, wamp.Dict{"username": username})
	if err != nil {
		return false, err
	}
	// If admin is found return true
	if len(res.Arguments) > 0 && res.Arguments[0] != nil && res.Arguments[0].(map[string]interface{})["username"].(string) == username {
		return true, nil
	}
	return false, nil
}

package helpers

import (
	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/service"
)

// Speak WAMP call helper to speak a message in chat
func Speak(channel string, message string) error {
	// Call twitch chat say endpoint
	_, err := service.Socket.SimpleCall("private.twitch.chat.say", wamp.List{channel, message}, nil)
	return err
}

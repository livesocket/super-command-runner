package helpers

import (
	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/service"
	"gitlab.com/livesocket/super-command-runner/models"
)

// FindSuperCommand WAMP call to find a super command by name
func FindSuperCommand(name string) (*models.SuperCommand, error) {
	// Call super command get
	res, err := service.Socket.SimpleCall("private.super.command.get", nil, wamp.Dict{"name": name})
	if err != nil {
		return nil, err
	}
	// Check and return response
	if len(res.Arguments) > 0 && res.Arguments[0] != nil {
		super := res.Arguments[0].(map[string]interface{})
		return &models.SuperCommand{
			Proc: super["proc"].(string),
			Name: super["name"].(string),
		}, nil
	}
	return nil, nil
}

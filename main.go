package main

import (
	"log"
	"os"
	"os/signal"

	"gitlab.com/livesocket/service"
	"gitlab.com/livesocket/service/lib"
	"gitlab.com/livesocket/super-command-runner/runner"
)

func main() {
	// Create service
	close, err := service.NewStandardService(nil, []lib.Subscription{runner.OnMessageSubscription}, "")
	defer close()
	if err != nil {
		panic(err)
	}

	// Wait for CTRL-c or client close while handling events.
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt)
	select {
	case <-sigChan:
	case <-service.Socket.Done():
		log.Print("Router gone, exiting")
		return
	}
}
